Korea Phone
============
This module is a Drupal field module that provides Korea's phone number input interface. The telephone number in Korea consists of the network identification number or area code, the base station number, and the serial number.
This module can be used to develop sites that are serviced in Korea. Use in other countries is inappropriate.

Installation
-------------
This is the same as a typical installation of the Drupal module.

Integration with Feeds module
------------------------------
Integration with the Feeds module is not yet available. We will support it in the future.