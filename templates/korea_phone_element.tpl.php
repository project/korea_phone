<?php
/**
 * Created by PhpStorm.
 * User: pig482
 * Date: 2017. 4. 30.
 * Time: PM 8:49
 */

$element = $variables['element'];
?>
<div id="<?=$element['#id']?>" class="stv_korea_address_widget">
    <?php print theme( 'form_element_label', $variables ); ?>
    <div class="stv_korea_address_wrapper">
        <?php print $element['#children']; ?>
    </div>
</div>